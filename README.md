# Cloudfoundry service broker for the graphdatabase neo4j #

This repository contains a cloudfoundry service broker for the graphdatabase neo4j

# Installation #

* Ensure that the latest Java and Maven versions are installed
* Clone this repo: `git clone https://bitbucket.org/Stumm190/cf-sb-neo4j.git`

# Usage #

Navigate to the directory into which you cloned the repo and execute this: `mvn spring-boot:run`

Once started you can access the APIs on port 8080, e.g. `http://localhost:8080/v2/catalog`

# Accessable endpoints #

_______________________

## ** /v2/catalog **


** `GET /v2/catalog` **

returns the service catalog:

|Path|Type|Description

|`services`
|`Array`
|The provided services

|`services[].id`
|`String`
|The ServiceDefinition ID

|`services[].name`
|`String`
|The ServiceDefinition Name

|`services[].description`
|`String`
|The ServiceDefinition description

|`services[].bindable`
|`Boolean`
|The ServiceDefinition bindable boolean

|`services[].plans[]`
|`Object`
|The ServiceDefinition plans

|`services[].plans[].id`
|`String`
|The ServiceDefinition plan id

|`services[].plans[].name`
|`String`
|The ServiceDefinition plans name

|`services[].plans[].description`
|`String`
|The ServiceDefinition plans description

______________

## ** /v2/service_instances/{instanceId} **



`PUT /v2/service_instances/{instanceId}`

For creating an instance


`DELETE /v2/service_instances/{instanceId}`

For deleting an instance


`PATCH /v2/service_instances/{instanceId}`

For updating an instance

__________

## ** /v2/service_instances/{instanceId}/service_bindings/{bindingId} **



`PUT /v2/service_instances/{instanceId}/service_bindings/{bindingId}`

For binding an instance

`DELETE /v2/service_instances/{instanceId}/service_bindings/{bindingId}`

For unbinding an instance