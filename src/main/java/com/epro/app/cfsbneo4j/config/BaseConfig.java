package com.epro.app.cfsbneo4j.config;

import java.io.IOException;
import java.util.Arrays;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.CustomClassLoaderConstructor;

import com.epro.app.cfsbneo4j.deployment.OpenstackPlatformService;
import com.epro.app.cfsbneo4j.model.Catalog;
import com.epro.app.cfsbneo4j.model.ServiceDefinition;

/**
 * Create beans: Catalog, ServiceDefinition, OpenstackPlatformService
 * @author Tobias, Yacine, Eric
 *
 */
@Configuration
public class BaseConfig {

	@Bean
	public Catalog catalog() {
		Catalog catalog = new Catalog(Arrays.asList(serviceDefinition()));

		return catalog;
	}

	@Bean
	public ServiceDefinition serviceDefinition() {
		ClassPathResource classPathResource = new ClassPathResource("/plans/service-definition.yml");
		CustomClassLoaderConstructor constructor = new CustomClassLoaderConstructor(ServiceDefinition.class, ServiceDefinition.class.getClassLoader());

		Yaml yaml = new Yaml(constructor);

		ServiceDefinition serviceDefinition = null;
		try {
			serviceDefinition = yaml.loadAs(classPathResource.getInputStream(), ServiceDefinition.class);
		} catch (IOException e) {
			e.printStackTrace();
		}

		return serviceDefinition;
	}

	@Bean
	public OpenstackPlatformService openstackPlatformService(){
		return new OpenstackPlatformService();
	}
		
}
