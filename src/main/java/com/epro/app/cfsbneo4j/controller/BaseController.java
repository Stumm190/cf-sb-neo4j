package com.epro.app.cfsbneo4j.controller;

import javax.servlet.http.HttpServletResponse;

import org.openstack4j.api.exceptions.ClientResponseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.epro.app.cfsbneo4j.model.ErrorMessage;

import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.http.HttpStatus;

/**
 * Handle exceptions while create/delete bindings and create/delete instances
 * @author Tobias, Yacine, Eric
 *
 */
public class BaseController {

	private final Logger logger = LoggerFactory.getLogger(BaseController.class);
	
	/**
	 * ExceptionHandler for HttpMessageNotReadableException
	 * @param ex The HttpMessageNotReadableException
	 * @param response The HttpServletResponse
	 * @return ErrorMessage with HttpStatus.UNPROCESSABLE_ENTITY and the Exceptionmessage
	 */
	@ExceptionHandler(HttpMessageNotReadableException.class)
	public ResponseEntity<ErrorMessage> handleException(HttpMessageNotReadableException ex, HttpServletResponse response) {
	    return getErrorResponse(ex.getMessage(), HttpStatus.UNPROCESSABLE_ENTITY);
	}
	
	/**
	 * ExceptionHandler for ClientResponseException
	 * @param ex The ClientResponseException
	 * @param response HttpServletResponse
	 * @return Statuscode and Exception message
	 */
	@ExceptionHandler(ClientResponseException.class)
	public ResponseEntity<ErrorMessage> handleException(ClientResponseException ex, HttpServletResponse response){
		logger.warn("ClientResponseException "+ex.getStatusCode(), ex);
		return getErrorResponse(ex.getMessage(), HttpStatus.UNPROCESSABLE_ENTITY);
	}
	
	/**
	 * ExceptionHandler for MethodArgumentNotValidException
	 * @param ex The MethodArgumentNotValidException
	 * @param response HttpServletResponse
	 * @return Missing required fields
	 */
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public ResponseEntity<ErrorMessage> handleException(MethodArgumentNotValidException ex, 
			HttpServletResponse response) {
	    BindingResult result = ex.getBindingResult();
	    String message = "Missing required fields:";
	    for (FieldError error: result.getFieldErrors()) {
	    	message += " " + error.getField();
	    }
		return getErrorResponse(message, HttpStatus.UNPROCESSABLE_ENTITY);
	}
	
	/**
	 * ExceptionHandler for Exception and /error endpoint
	 * @param ex Exception
	 * @param response HttpServletResponse
	 * @return Internal server error
	 */
	@ExceptionHandler(Exception.class)
	@RequestMapping(value = { "/error" }, method = RequestMethod.GET)
	public ResponseEntity<ErrorMessage> handleException(Exception ex, 
			HttpServletResponse response) {
		logger.warn("Exception", ex);
	    return getErrorResponse(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	/**
	 * Custom ErrorResponse method
	 * @param message The Message String
	 * @param status The HttpStatus
	 * @return Message and HttpStatus as a ErrorMessage
	 */
	public ResponseEntity<ErrorMessage> getErrorResponse(String message, HttpStatus status) {
		return new ResponseEntity<ErrorMessage>(new ErrorMessage(message), status);
	}

}
