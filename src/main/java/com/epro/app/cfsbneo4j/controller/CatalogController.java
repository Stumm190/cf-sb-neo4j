package com.epro.app.cfsbneo4j.controller;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.epro.app.cfsbneo4j.model.Catalog;
import com.epro.app.cfsbneo4j.service.CatalogService;

/**
 * Handle request method GET and mapped the value /
 * @author Tobias, Yacine, Eric
 *
 */
@Controller
@RequestMapping(value = "/v2/catalog")
public class CatalogController extends BaseController {

	@Autowired 
	private CatalogService service;
	
	/**
	 * /v2/catalog endpoint. 
	 * @return The service catalag with all provided plans.
	 * @throws IOException For all IOExceptions
	 * @throws InterruptedException For all InterruptedExceptions
	 */
	@RequestMapping(value = { "/", "" }, method = RequestMethod.GET)
	public @ResponseBody Catalog getCatalog() throws IOException, InterruptedException {
		return service.getCatalog();
	}
	
}
