package com.epro.app.cfsbneo4j.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.epro.app.cfsbneo4j.model.ServiceBinding;
import com.epro.app.cfsbneo4j.model.ServiceInstance;
import com.epro.app.cfsbneo4j.service.ServiceBindingService;

/**
 * Handle PUT and DELETE requests to create or delete bindings to an existing instance
 * @author Tobias, Yacine, Eric
 *
 */
@Controller
@RequestMapping(value = "/v2/service_instances/{instanceId}/service_bindings/{bindingId}")
public class ServiceBindingController extends BaseController {
	
	@Autowired
	private ServiceBindingService bindingService;
	
	/**
	 * PUT endpoint for /v2/service_instances/{instanceId}/service_bindings/{bindingId}
	 * @param instanceId The ID of the Instance that will be binded
	 * @param bindingId The ID of the binding that will be created
	 * @return The created binding. If binding already exists, HttpStatus.OK, else HttpStatus.CREATED
	 */
	@RequestMapping(method = RequestMethod.PUT)
	@ResponseBody
	public ResponseEntity<ServiceBinding> update(@PathVariable String instanceId, @PathVariable String bindingId) {
		ServiceBinding binding = null;
		if(ServiceBindingService.serviceBindingContainer.get("instance"+instanceId+"binding"+bindingId)!=null){
			return new ResponseEntity<ServiceBinding>(ServiceBindingService.serviceBindingContainer.get("instance"+instanceId+"binding"+bindingId), HttpStatus.OK);
		}
		binding = new ServiceBinding(bindingId, ServiceInstance.serviceInstanceContainer.get(instanceId));
		ServiceBindingService.serviceBindingContainer.put("instance"+instanceId+"binding"+bindingId, binding);
		bindingService.save(ServiceBindingService.serviceBindingContainer.get("instance"+instanceId+"binding"+bindingId));
		return new ResponseEntity<ServiceBinding>(binding, HttpStatus.CREATED); 
	}
	
	/**
	 * DELTE endpoint for /v2/service_instances/{instanceId}/service_bindings/{bindingId}
	 * @param instanceId The ID of the instance that is contained in the binding
	 * @param bindingId The ID of the binding that will be deleted
	 * @return If binding doesn't exists, HttpStatus.GONE, else HttpStatus.OK
	 */
	@RequestMapping(method = RequestMethod.DELETE)
	@ResponseBody
	public ResponseEntity<String> destroy(@PathVariable String instanceId, @PathVariable String bindingId) {
		if(ServiceBindingService.serviceBindingContainer.get("instance"+instanceId+"binding"+bindingId)==null){
			return new ResponseEntity<String>(HttpStatus.GONE);
		}
		bindingService.destroy(ServiceBindingService.serviceBindingContainer.remove("instance"+instanceId+"binding"+bindingId));
	    return new ResponseEntity<String>(HttpStatus.OK);
	}
}
