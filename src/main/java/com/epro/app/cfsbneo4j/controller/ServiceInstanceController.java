package com.epro.app.cfsbneo4j.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.epro.app.cfsbneo4j.model.ServiceInstance;
import com.epro.app.cfsbneo4j.model.ServiceInstanceRequest;
import com.epro.app.cfsbneo4j.service.ServiceInstanceService;

/**
 * Handle PUT, DELETE and PATCH requests to create, delete or update instances
 * @author Tobias, Yacine, Eric
 *
 */
@Controller
@RequestMapping("/v2/service_instances/{instanceId}")
public class ServiceInstanceController extends BaseController {

	@Autowired
	private ServiceInstanceService instanceService;
	
	  /**
	   * PUT endpoint for /v2/service_instances/{instanceId} for creating an instance
	   * @param instanceId The ID of the instance that will be created
	   * @param request Contains the requestbody of the request
	   * @return If instance already exists, HttpStatus.OK, else HttpStatus.Created
	   */
	  @RequestMapping(method = RequestMethod.PUT)
	  @ResponseBody
	  public ResponseEntity<String> createServiceInstance(@PathVariable("instanceId") String instanceId, 
			  @RequestBody(required=false) ServiceInstanceRequest request) {
		  if(ServiceInstance.serviceInstanceContainer.get(instanceId)!=null)
			  return new ResponseEntity<String>("Instance already exists", HttpStatus.OK);
		  
		  instanceService.create(instanceId, (request == null)? "" : request.getPlanID());
			  
		 
		  return new ResponseEntity<String>("Instance created", HttpStatus.CREATED);
	  }
	  
	  /**
	   * DELETE endpoint for /v2/service_instances/{instanceId} for deleting an Instance
	   * @param instanceId The ID of the Instance that will be deleted
	   * @return If Instance doesn't exists, HttpStatus.GONE, else HttpStatus.OK
	   */
	  @RequestMapping(method = RequestMethod.DELETE)
	  @ResponseBody
	  public ResponseEntity<String> destroy(@PathVariable String instanceId) {
		  if(ServiceInstance.serviceInstanceContainer.get(instanceId)==null)
			  return new ResponseEntity<String>("No Intance found to delete", HttpStatus.GONE);
		  instanceService.delete(instanceId);
	    return new ResponseEntity<String>("Instance deleted", HttpStatus.OK);
	  }
	  
	  /**
	   * PATCH endpoint for /v2/service_instances/{instanceId} for updating an instance
	   * @param instanceId The ID of the instance that should be updated
	   * @return HttpStatus.OK
	   */
	  @RequestMapping(method = RequestMethod.PATCH)
	  @ResponseBody
	  public ResponseEntity<String> update(@PathVariable("instanceId") String instanceId){
		  return new ResponseEntity<String>("Intance updated" ,HttpStatus.OK);
	  }
}
