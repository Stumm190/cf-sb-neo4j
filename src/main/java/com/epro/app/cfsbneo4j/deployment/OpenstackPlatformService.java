package com.epro.app.cfsbneo4j.deployment;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.openstack4j.api.Builders;
import org.openstack4j.api.OSClient.OSClientV3;
import org.openstack4j.model.common.Identifier;
import org.openstack4j.model.compute.Server;
import org.openstack4j.model.heat.Stack;
import org.openstack4j.openstack.OSFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;

import com.epro.app.cfsbneo4j.model.Plan;
import com.epro.app.cfsbneo4j.model.ServiceInstance;
import com.epro.app.cfsbneo4j.observer.StackProgressObserver;

/**
 * Handle the authentication to openstack and create/delete stacks
 * @author Tobias, Yacine, Eric
 *
 */
@Service
public class OpenstackPlatformService {
	
	private static Map<String, Stack> createdStacks = new HashMap<String, Stack>();
	
	@Autowired
	private StackProgressObserver spo;
	
	/**
	 * Method for authentication at openstack
	 * @return The OSClientV3 with the current session
	 */
	public OSClientV3 authenticate() {
		Identifier dId = Identifier.byName("evoila");
		OSClientV3 os = OSFactory.builderV3()
	            .endpoint("https://ui.os.onstack.de:5000/v3")
	            .credentials("group3", "studentOS", dId)
	            .scopeToProject(Identifier.byName("epro"), Identifier.byName("evoila"))
	            .authenticate();
		return os;
	}	
	
	/**
	 * Method for creating a stack 
	 * @param os The OSClientV3 that contains the current session
	 * @param instanceId The ID of the stack
	 * @param plan The plan ID for the stack
	 */
	public void createStack(OSClientV3 os, String instanceId, Plan plan){
		File file;
		String templateString = "";
		try {
			file = ResourceUtils.getFile("classpath:deployment/template.yml");
			templateString = new String(Files.readAllBytes(file.toPath()));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		Map<String, String> defParams = new HashMap<String, String>();
		
		if(plan != null) {
			switch(plan.getName()) {
			case "S":
				defParams.put("flavor", "gp1.tiny");
				break;
			case "M":
				defParams.put("flavor", "gp1.small");
				break;
			case "XL":
				defParams.put("flavor", "cb1.small");
				break;
			}
		}
		
		Stack stack = os.heat().stacks().create(Builders.stack()
                .name("Neo4j-Stack-"+instanceId)
                .template(templateString)
                .parameters(defParams)
                .timeoutMins(5l).build());
		
		spo.stackObserver(os, "Neo4j-Stack-"+instanceId);

		List<? extends Server> servers = authenticate().compute().servers().list();
		Server newServer = null;
		for(int i = 0;i<servers.size();i++){
			if(servers.get(i).getName().startsWith("Neo4j-Stack-"+instanceId)){
				newServer = servers.get(i);
			} 
		}

		createdStacks.put(instanceId, stack);
		ServiceInstance.serviceInstanceContainer.put(instanceId,new ServiceInstance(instanceId, 
				newServer.getAddresses().getAddresses().get("intern").get(1).getAddr()));
		spo.serverIsAvailable(newServer.getAddresses().getAddresses().get("intern").get(1).getAddr());
	}
	
	/**
	 * Method for deleting a stack
	 * @param os The OSClientV3 that contains the current session
	 * @param instanceId The ID of the stack/instance
	 */
	public void deleteStack(OSClientV3 os, String instanceId){
		os.heat().stacks().delete("Neo4j-Stack-"+instanceId,createdStacks.remove(instanceId).getId());
		ServiceInstance.serviceInstanceContainer.remove(instanceId);
	}
}
