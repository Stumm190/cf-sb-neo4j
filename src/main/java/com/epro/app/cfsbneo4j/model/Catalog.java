package com.epro.app.cfsbneo4j.model;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * The representation of a catalog
 * @author Tobias, Yacine, Eric
 */
@JsonAutoDetect(getterVisibility = Visibility.NONE)
public class Catalog {

	@JsonSerialize
	@JsonProperty("services")
	private List<ServiceDefinition> serviceDefinitions = new ArrayList<ServiceDefinition>();
	
	/**
	 * CustomConstructor to set a ServiceDefinition
	 * @param serviceDefinitions The list of servicedefinitions that will be set for the catalog
	 */
	public Catalog(List<ServiceDefinition> serviceDefinitions) {
		this.setServiceDefinitions(serviceDefinitions); 
	}
	
	/**
	 * Getter for the ServiceDefinition
	 * @return The ServiceDefinition 
	 */
	public List<ServiceDefinition> getServiceDefinitions() {
		return serviceDefinitions;
	}

	/**
	 * Setter for ServiceDefinition
	 * @param serviceDefinitions The list of servicedefinitions that will be set for the catalog
	 */
	private void setServiceDefinitions(List<ServiceDefinition> serviceDefinitions) {
		if ( serviceDefinitions == null ) {
			this.serviceDefinitions = new ArrayList<ServiceDefinition>();
		} else {
			this.serviceDefinitions = serviceDefinitions;
		} 
	}
	
}
