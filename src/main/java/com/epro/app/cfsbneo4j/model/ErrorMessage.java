package com.epro.app.cfsbneo4j.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Class for a custom ErrorMessage
 * @author Tobias, Yacine, Eric
 */
public class ErrorMessage {

	@JsonProperty("message")
	private String message;

	/**
	 * CustomConstructor for setting a message
	 * @param message The message string
	 */
	public ErrorMessage(String message) {
		this.message = message;
	}

	/**
	 * Getter for the Message
	 * @return The message
	 */
	public String getMessage() {
		return message;
	}
	
	/**
	 * Setter for the Message
	 * @param message The message string
	 */
	public void setMessage(String message) {
		this.message = message;
}
}
