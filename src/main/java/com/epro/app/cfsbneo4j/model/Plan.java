package com.epro.app.cfsbneo4j.model;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * The representation of a plan
 * @author Tobias, Yacine, Eric
 */
@JsonAutoDetect(getterVisibility = Visibility.NONE)
public class Plan {

	@JsonSerialize
	@JsonProperty("id")
	private String id;

	@JsonSerialize
	@JsonProperty("name")
	private String name;

	@JsonSerialize
	@JsonProperty("description")
	private String description;

	public Plan() {
		super();
	}

	/**
	 * CustomConstructor for Plan
	 * @param id The plan ID
	 * @param name The plan name
	 * @param description The description of the plan
	 */
	public Plan(String id, String name, String description) {
		this.id = id;
		this.name = name;
		this.description = description;
	}
	
	/**
	 * Getter for the id of the plan
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * Setter for id of the plan
	 * @param id The plan ID
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * Getter for name of the plan
	 * @return The plan name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Setter for name of the plan
	 * @param name The plan name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Getter for the description of the plan
	 * @return The description string
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Setter for the description of the plan
	 * @param description The description string
	 */
	public void setDescription(String description) {
		this.description = description;
	}
}
