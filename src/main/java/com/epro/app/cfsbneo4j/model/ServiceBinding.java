package com.epro.app.cfsbneo4j.model;

import java.util.UUID;

/**
 * The reprentation of a ServiceBinding
 * @author Tobias, Yacine, Eric
 */
public class ServiceBinding {
	private String username, password, host, url, port, database;
	
	/**
	 * CustomConstructor of ServiceBinding
	 * @param id The binding ID
	 * @param instance The ServiceInstance that will be binded
	 */
	public ServiceBinding(String id, ServiceInstance instance) {
		this.username = "user"+id;
		this.password = UUID.randomUUID().toString();
		this.host = "";
		this.url = instance.getDatabase()+":7687";
		this.port = "7687"; 
		this.database = instance.getDatabase();
	}

	/**
	 * Getter for the username of the binding
	 * @return The username string
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * Setter for the Neo4j-database Username of the binding
	 * @param username The username string
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * Getter for the Neo4j-database password of the binding
	 * @return The password string
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * Setter for the Neo4j-database password of the binding
	 * @param password The password string
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * Getter for the host of the binding
	 * @return The host string
	 */
	public String getHost() {
		return host;
	}

	/**
	 * Setter for the Host of the binding
	 * @param host The host
	 */
	public void setHost(String host) {
		this.host = host;
	}

	/**
	 * Getter for the database url of the binding
	 * @return The url string
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * Setter for the database url of the binding
	 * @param url The url string
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * Getter for the port of the binding
	 * @return The port
	 */
	public String getPort() {
		return port;
	}
	
	/**
	 * Setter for the database port of the binding
	 * @param port The port string
	 */
	public void setPort(String port) {
		this.port = port;
	}

	/**
	 * Getter of the database IP
	 * @return The database IP string
	 */
	public String getDatabase() {
		return database;
	}

	/**
	 * Setter for the database IP
	 * @param database The database IP string
	 */
	public void setDatabase(String database) {
		this.database = database;
	}
	
}
