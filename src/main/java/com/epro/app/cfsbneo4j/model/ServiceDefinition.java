package com.epro.app.cfsbneo4j.model;

import java.util.ArrayList;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * The ServiceDefinition representation
 * @author Tobias, Yacine, Eric
 */
@JsonAutoDetect(getterVisibility = Visibility.NONE)
public class ServiceDefinition {

	@JsonSerialize
	@JsonProperty("id")
	private String id;

	@JsonSerialize
	@JsonProperty("name")
	private String name;

	@JsonSerialize
	@JsonProperty("description")
	private String description;

	@JsonSerialize
	@JsonProperty("bindable")
	private boolean bindable;

	@JsonSerialize
	@JsonProperty("plans")
	private List<Plan> plans = new ArrayList<Plan>();

	public ServiceDefinition() {
		super();
	}

	/**
	 * CustomConstructor of ServiceDefinition
	 * @param id The ID of the ServiceDefinition
	 * @param name The name of the ServiceDefinition
	 * @param description The description of the ServiceDefinition
	 * @param bindable The bindable boolean of the ServiceDefinition
	 * @param plans The plan list of the ServiceDefinition
	 */
	public ServiceDefinition(String id, String name, String description, boolean bindable, List<Plan> plans) {
		this.id = id;
		this.name = name;
		this.description = description;
		this.bindable = bindable;
		this.setPlans(plans);
	}
	
	/**
	 * Getter for id
	 * @return The ServiceDefinition ID
	 */
	public String getId() {
		return id;
	}

	/**
	 * Setter for id
	 * @param id The ServiceDefinition ID
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * Getter for the name of the ServiceDefinition
	 * @return The ServiceDefinition name string
	 */
	public String getName() {
		return name;
	}

	/**
	 * Setter for the name of the ServiceDefinition
	 * @param name The ServiceDefinition name string
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Getter for the description of the ServiceDefinition
	 * @return The description string of the ServiceDefinition
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Setter for the description of the ServiceDefinition
	 * @param description The description string of the ServiceDefinition
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Gets if the service is bindable
	 * @return The bindable boolean
	 */
	public boolean isBindable() {
		return bindable;
	}

	/**
	 * Setter for the bindable boolean
	 * @param bindable The bindable boolean
	 */
	public void setBindable(boolean bindable) {
		this.bindable = bindable;
	}

	/**
	 * Gets a list of all plans
	 * @return The plan list of the ServiceDefinition
	 */
	public List<Plan> getPlans() {
		return plans;
	}

	/**
	 * Setter for the plan list
	 * @param plans The list of the plans of the ServiceDefinition
	 */
	public void setPlans(List<Plan> plans) {
		if (plans == null) {
			this.plans = new ArrayList<Plan>();
		} else {
			this.plans = plans;
		}
	}
	
	/**
	 * Getter for a single plan 
	 * @param planId The plan ID
	 * @return The plan with the given ID
	 */
	public Plan getPlan(String planId){
		for(int i=0;i<plans.size();i++){
			if(plans.get(i).getId().equals(planId))
				return plans.get(i);
		}
		return null;
	}
}
