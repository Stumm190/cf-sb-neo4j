package com.epro.app.cfsbneo4j.model;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * The representation of an instance
 * @author Tobias, Yacine, Eric
 */
public class ServiceInstance {

	private String database;
	public static Map<String, ServiceInstance> serviceInstanceContainer = new HashMap<String, ServiceInstance>();

	@JsonSerialize
	@JsonProperty("service_instance_id")
	private String id;

	/**
	 * CustomCOnstructor of the ServiceInstance
	 * @param serviceInstanceId The ID of the ServiceInstance
	 * @param database The database IP string for the ServiceInstance
	 */
	public ServiceInstance(String serviceInstanceId, String database) {
		this.id = serviceInstanceId;
		this.database = database;
		
	}

	/**
	 * CustomConstructor of ServiceInstane for setting the database id
	 * @param id The Service instance ID string
	 */
	public ServiceInstance(String id) {
		setDatabase(id);
	}

	/**
	 * Setter for the ServiceInstance id
	 * @return The ServiceInstance ID string
	 */
	public String getId() {
		return id;
	}

	/**
	 * Setter for the ServiceInstance id
	 * @param id The SerivceInstance ID string
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * Getter for the database of the ServiceInstance
	 * @return The database IP string
	 */
	public String getDatabase() {
		return database;
	}

	/**
	 * Setter for the database of the ServiceInstance
	 * @param database The database IP string
	 */
	public void setDatabase(String database) {
		this.database = database;
	}
}
