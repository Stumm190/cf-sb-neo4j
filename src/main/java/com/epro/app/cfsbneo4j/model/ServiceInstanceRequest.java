package com.epro.app.cfsbneo4j.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * 
 * @author Eric, Tobias, Yacine
 *
 */

public class ServiceInstanceRequest {

	@JsonSerialize
	@JsonProperty("plan_id")
	private String planID;
	
	public ServiceInstanceRequest(String planID) {
		this.planID = planID;
	}
	
	public String getPlanID() {
		return planID;
	}
}
