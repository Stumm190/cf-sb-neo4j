package com.epro.app.cfsbneo4j.observer;

import org.openstack4j.model.heat.Stack;
import org.neo4j.driver.v1.AuthTokens;
import org.neo4j.driver.v1.Driver;
import org.neo4j.driver.v1.GraphDatabase;
import org.neo4j.driver.v1.Session;
import org.neo4j.driver.v1.exceptions.ServiceUnavailableException;
import org.openstack4j.api.OSClient.OSClientV3;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * Observer the installation and the reboot from the server 
 * @author Tobias, Yacine, Eric
 *
 */
@Service
public class StackProgressObserver {

	private Logger log = LoggerFactory.getLogger(getClass());
	
	/**
	 * Method for waiting for completion of the creation of the stack
	 * @param os The OSClientV3 that contains the current session
	 * @param stackName The stack name
	 */
	public void stackObserver(OSClientV3 os, String stackName) {
		Stack stack;
		
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		stack = os.heat().stacks().getStackByName(stackName);
		
		log.info("Install and configurate Neo4j");
		
		while(stack.getStatus().equals("CREATE_IN_PROGRESS")) {
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			stack = os.heat().stacks().getStackByName(stackName);
		}
		
		log.info("Neo4j installation and configuration are complete");
		log.info("Restart server");
	}
	
	/**
	 * Method for waiting till the server is available
	 * @param floatingIP The floatingIP string of the server
	 */
	public void serverIsAvailable(String floatingIP) {
		boolean isAvailable = false;
		
		while(!isAvailable) {
			try {
					Thread.sleep(5000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			try {
				Driver driver = GraphDatabase.driver( "bolt://"+floatingIP+":7687", AuthTokens.basic( "neo4j", "neo4j") );
				Session session = driver.session();

				session.close();
				driver.close();
				isAvailable = true;
			} catch(ServiceUnavailableException sue) {
				
			}
			log.info("Server is now available :)");
		}
	}
}
