package com.epro.app.cfsbneo4j.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epro.app.cfsbneo4j.model.Catalog;

/**
 * @author Tobias, Yacine, Eric
 *
 */
@Service
public class CatalogService {

	@Autowired
	private Catalog catalog;
	
	/**
	 * Getter for the catalog
	 * @return The catalog
	 */
	public Catalog getCatalog() {
		return catalog;
	}

}
