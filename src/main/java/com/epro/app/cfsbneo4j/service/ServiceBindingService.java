package com.epro.app.cfsbneo4j.service;

import java.util.HashMap;
import java.util.Map;

import org.neo4j.driver.v1.AuthTokens;
import org.neo4j.driver.v1.Driver;
import org.neo4j.driver.v1.GraphDatabase;
import org.neo4j.driver.v1.Session;
import org.springframework.stereotype.Service;

import com.epro.app.cfsbneo4j.model.ServiceBinding;

/**
 * Create or destroy users from the neo4j database 
 * @author Tobias, Yacine, Eric
 *
 */
@Service
public class ServiceBindingService {
	
	public static Map<String, ServiceBinding> serviceBindingContainer = new HashMap<String, ServiceBinding>();
	
	/**
	 * Method for creating the database user of the binding
	 * @param binding The ServiceBinding 
	 */
	public void save(ServiceBinding binding) {
		
		Driver driver = GraphDatabase.driver( "bolt://"+binding.getDatabase()+":7687", AuthTokens.basic( "neo4j", "neo4j") );
		Session session = driver.session();

		session.run("CALL dbms.security.createUser('" + binding.getUsername()+"', '" + binding.getPassword() + "', false);");
		session.close();
		driver.close();
	}
	
	/**
	 * Method for deleting the database user of the binding
	 * @param binding The ServiceBinding
	 */
	public void destroy(ServiceBinding binding) {
		Driver driver = GraphDatabase.driver( "bolt://"+binding.getDatabase()+":7687", AuthTokens.basic( "neo4j", "neo4j") );
		Session session = driver.session();

		session.run("CALL dbms.security.deleteUser('" + binding.getUsername() + "')");
		session.close();
		driver.close();
	}
}
