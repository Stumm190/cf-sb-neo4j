package com.epro.app.cfsbneo4j.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epro.app.cfsbneo4j.deployment.OpenstackPlatformService;
import com.epro.app.cfsbneo4j.model.ServiceDefinition;

/**
 * Call the create or delete function from the OpenstackPlatformService class
 * @author Tobias, Yacine, Eric
 *
 */
@Service
public class ServiceInstanceService {
	
	@Autowired
	private OpenstackPlatformService ops;
		
	@Autowired
	private ServiceDefinition serviceDefinition;
	
	/**
	 * Method for creating a stack. Calls the OpenstackPlatformService createStack method
	 * @param instanceId The ServiceInstance ID string that will be created
	 * @param planID The plan ID string for the ServiceInstance
	 */
	public void create(String instanceId, String planID) {
		ops.createStack(ops.authenticate(), instanceId, serviceDefinition.getPlan(planID));
	}
	
	/**
	 * Method for deleting a stack. Calls the OpenstackPlatformService deleteStack method
	 * @param instanceId The ID string of the ServiceInstance that will be deleted
	 */
	public void delete(String instanceId) {
		ops.deleteStack(ops.authenticate(), instanceId);
	}
}
