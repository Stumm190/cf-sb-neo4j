cd ~
wget -O - http://debian.neo4j.org/neotechnology.gpg.key >> key.pgp
apt-key add key.pgp
echo 'deb http://debian.neo4j.org/repo stable/'| tee -a /etc/apt/sources.list.d/neo4j.list > /dev/null
apt-get update
apt-get install -y neo4j
echo 'Install finished'
service neo4j start
echo 'Service started'
#Path to database
mkdir -p ~/neo4j-database/database
#Start neo4j shell
neo4j-shell -path ~/neo4j-database/database
echo 'Create database'
