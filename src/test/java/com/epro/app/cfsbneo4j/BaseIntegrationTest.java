package com.epro.app.cfsbneo4j;

import java.util.Arrays;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

import com.epro.app.cfsbneo4j.controller.CatalogController;
import com.epro.app.cfsbneo4j.model.Catalog;
import com.epro.app.cfsbneo4j.model.Plan;
import com.epro.app.cfsbneo4j.model.ServiceDefinition;

/**
 * @author Tobias, Yacine, Eric
 *
 */
@ComponentScan(basePackages = { "com.epro.app.cfsbneo4j"})
public class BaseIntegrationTest{
	
	protected Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Bean
	public Catalog catalog() {
		Catalog catalog = new Catalog(Arrays.asList(serviceDefinition()));

		return catalog;
	}
	
	@Bean
	public CatalogController CatalogController(){
		return new CatalogController();
	}
	
	@Bean
	public ServiceDefinition serviceDefinition() {
		
		Plan openstackPlan = new Plan("neo4j-s", "S", "The most basic Neo4j plan currently available."
				+ " Providing 20 GB of capacity in a Neo4j DB");

		ServiceDefinition serviceDefinition = new ServiceDefinition("neo4j", "neo4j", "neo4j Instances",
				true, Arrays.asList(openstackPlan));

		return serviceDefinition;
	}
	
	
	
	
}
