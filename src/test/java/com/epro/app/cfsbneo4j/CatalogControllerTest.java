package com.epro.app.cfsbneo4j;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import java.util.Arrays;
import java.util.List;


import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.core.Is.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


import com.epro.app.cfsbneo4j.controller.CatalogController;
import com.epro.app.cfsbneo4j.model.Catalog;
import com.epro.app.cfsbneo4j.model.Plan;
import com.epro.app.cfsbneo4j.model.ServiceDefinition;


/**
 * @author Tobias, Yacine, Eric
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ComponentScan(basePackages = { "com.epro.app.cfsbneo4j"})
public class CatalogControllerTest{

	private MockMvc mockMvc;
	
	 @Mock
	 private Catalog catalog;

    @Mock
    private CatalogController catalogController;
    
    @Before
    public void init(){
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders
                .standaloneSetup(catalogController)
                .build();
    }
    
    @Test
    public void getCatalogTest() throws Exception {
    	
    	Plan openstackPlan = new Plan("neo4j-s", "S", "The most basic Neo4j plan currently available."
				+ " Providing 20 GB of capacity in a Neo4j DB");

    	List<ServiceDefinition> serviceDefinitions = Arrays.asList(
				new ServiceDefinition("neo4j", "neo4j", "neo4j Instances",
						true, Arrays.asList(openstackPlan)));
		
		catalog = new Catalog(serviceDefinitions);  
		
    	when(catalogController.getCatalog()).thenReturn(catalog);
        mockMvc.perform(get("/v2/catalog").accept(MediaType.APPLICATION_JSON_UTF8))
        		.andDo(print())
        		.andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.services", hasSize(1)))
             	.andExpect(jsonPath("$.services[0].id", is(serviceDefinitions.get(0).getId())))
             	.andExpect(jsonPath("$.services[0].name", is(serviceDefinitions.get(0).getName())))
             	.andExpect(jsonPath("$.services[0].description", is(serviceDefinitions.get(0).getDescription())))
             	.andExpect(jsonPath("$.services[0].bindable", is(serviceDefinitions.get(0).isBindable())))
             	.andExpect(jsonPath("$.services[0].plans[0].id", 
             			is(serviceDefinitions.get(0).getPlans().get(0).getId())))
				.andExpect(jsonPath("$.services[0].plans[0].name", 
						is(serviceDefinitions.get(0).getPlans().get(0).getName())))
				.andExpect(jsonPath("$.services[0].plans[0].description", 
						is(serviceDefinitions.get(0).getPlans().get(0).getDescription())));
        }
}
