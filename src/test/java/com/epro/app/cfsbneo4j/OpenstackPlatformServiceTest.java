package com.epro.app.cfsbneo4j;



import static org.junit.Assert.assertNotNull;

import java.io.IOException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;

import org.junit.Before;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.epro.app.cfsbneo4j.deployment.OpenstackPlatformService;
import com.epro.app.cfsbneo4j.model.ServiceInstance;

/**
 * @author Tobias, Yacine, Eric
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = BaseIntegrationTest.class)
@ComponentScan(basePackages = { "com.epro.app.cfsbneo4j"})
public class OpenstackPlatformServiceTest{

	@Mock
	private ServiceInstance serviceInstance;
	
	
	@Mock
	private OpenstackPlatformService openstackPlatformService;
	
	
	
	@Before
	public void before() {
		serviceInstance = new ServiceInstance("instanceID", "dataBase");
	}
	
	@Test
	public void authenticateTest() throws IOException, InterruptedException{
		assertNotNull(openstackPlatformService.authenticate());
	}
	
	
	@Test
	public void createInstanceTest() throws IOException, InterruptedException{
		openstackPlatformService.createStack(openstackPlatformService.authenticate(), serviceInstance.getId(), null);
		
	}
	
	
	@Test
	public void deleteInstanceTest() throws IOException, InterruptedException{
		openstackPlatformService.deleteStack(openstackPlatformService.authenticate(), serviceInstance.getId());
	}
	
	

}
