package com.epro.app.cfsbneo4j.springrestdoc;

import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath;
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.request.RequestDocumentation.pathParameters;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.delete;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.patch;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.restdocs.JUnitRestDocumentation;
import org.springframework.restdocs.payload.JsonFieldType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.epro.app.cfsbneo4j.Application;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = Application.class)
@WebAppConfiguration
public class SpringRestDocsAPIDocumentationTest{
	@Rule
	public final JUnitRestDocumentation restDocumentation = new JUnitRestDocumentation("target/generated-snippets");

	@Autowired
	private WebApplicationContext context;

	private MockMvc mockMvc;

	@Before
	public void setUp() {
		System.out.println("beforeMethod");
		this.mockMvc = MockMvcBuilders.webAppContextSetup(this.context)
				.apply(documentationConfiguration(this.restDocumentation))
				.alwaysDo(document("{method-name}/{step}/"))
				.build();
	}
    
    
    @Test
    public void testAndDocumentServiceBroker() throws Exception {
        this.mockMvc.perform(get("/v2/catalog/"))
                .andExpect(status().isOk())
                .andDo(document("catalog", responseFields(fieldWithPath("services")
                				.type(JsonFieldType.ARRAY) 
                				.description("The provided services"),
                				fieldWithPath("services[].id")
                				.type(JsonFieldType.STRING) 
                				.description("The ServiceDefinition ID"),
                				fieldWithPath("services[].name")
                				.type(JsonFieldType.STRING) 
                				.description("The ServiceDefinition Name"),
                				fieldWithPath("services[].description")
                				.type(JsonFieldType.STRING) 
                				.description("The ServiceDefinition description"),
                				fieldWithPath("services[].bindable")
                				.type(JsonFieldType.BOOLEAN) 
                				.description("The ServiceDefinition bindable boolean"),
                				fieldWithPath("services[].plans[]")
                				.type(JsonFieldType.OBJECT) 
                				.description("The ServiceDefinition plans"),
                				fieldWithPath("services[].plans[].id")
                				.type(JsonFieldType.STRING) 
                				.description("The ServiceDefinition plan id"),
                				fieldWithPath("services[].plans[].name")
                				.type(JsonFieldType.STRING) 
                				.description("The ServiceDefinition plans name"),
                				fieldWithPath("services[].plans[].description")
                				.type(JsonFieldType.STRING) 
                				.description("The ServiceDefinition plans description"))));
        
        this.mockMvc.perform(put("/v2/service_instances/{instanceId}","SpringRestDocTest").accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isCreated())
        .andDo(document("putInstance", pathParameters( 
 			parameterWithName("instanceId").description("The ID of the instance that will be created"))));

        this.mockMvc.perform(put("/v2/service_instances/{instanceId}/service_bindings/{bindingId}","SpringRestDocTest",1))
        .andExpect(status().isCreated())
        .andDo(document("putServiceBinding", pathParameters( 
    			parameterWithName("instanceId").description("The ID of the instance that will be created"),
    			parameterWithName("bindingId").description("The ID of the binding that will be created"))));
       
        this.mockMvc.perform(delete("/v2/service_instances/{instanceId}/service_bindings/{bindingId}","SpringRestDocTest",1))
        .andExpect(status().isOk())
        .andDo(document("deleteServiceBinding", pathParameters( 
    			parameterWithName("instanceId").description("The ID of the instance that will be created"),
    			parameterWithName("bindingId").description("The ID of the binding that will be created"))));
        
        this.mockMvc.perform(patch("/v2/service_instances/{instanceId}","SpringRestDocTest"))
        .andExpect(status().isOk())
        .andDo(document("patchInstance", pathParameters( 
    			parameterWithName("instanceId").description("The ID of the instance that will be created"))));
        
        this.mockMvc.perform(delete("/v2/service_instances/{instanceId}","SpringRestDocTest"))
        .andExpect(status().isOk())
        .andDo(document("deleteInstance", pathParameters( 
    			parameterWithName("instanceId").description("The ID of the instance that will be created"))));
        
    }
//    
//    @Test
//    public void testAndDocumentputServiceInstance() throws Exception {
//        this.mockMvc.perform(put("http://localhost:8080/v2/service_instances/{instanceId}",1).accept(MediaType.APPLICATION_JSON))
//                .andExpect(status().isCreated())
//                .andDo(document("index", pathParameters( 
//            			parameterWithName("instanceId").description("The ID of the instance that will be created"))));
//    }
//    
//    @Test
//    public void testAndDocumentputServiceBinding() throws Exception {
//        this.mockMvc.perform(put("http://localhost:8080/v2/service_instances/{instanceId}/service_bindings/{bindingId}",1,1))
//                .andExpect(status().isCreated())
//                .andDo(document("index", pathParameters( 
//            			parameterWithName("instanceId").description("The ID of the instance that will be created"),
//            			parameterWithName("bindingId").description("The ID of the binding that will be created"))));
//    }
//    
//    @Test
//    public void testAndDocumentdeleteServiceBinding() throws Exception {
//        this.mockMvc.perform(delete("http://localhost:8080/v2/service_instances/{instanceId}/service_bindings/{bindingId}",1,1))
//                .andExpect(status().isOk())
//                .andDo(document("index", pathParameters( 
//            			parameterWithName("instanceId").description("The ID of the instance that will be created"),
//            			parameterWithName("bindingId").description("The ID of the binding that will be created"))));
//    }
//    
//    @Test
//    public void testAndDocumentpatchServiceInstance() throws Exception {
//        this.mockMvc.perform(patch("http://localhost:8080/v2/service_instances/{instanceId}",1))
//                .andExpect(status().isOk())
//                .andDo(document("index", pathParameters( 
//            			parameterWithName("instanceId").description("The ID of the instance that will be created"))));
//    }
//    
//    @Test
//    public void testAndDocumentdeleteServiceInstance() throws Exception {
//        this.mockMvc.perform(delete("http://localhost:8080/v2/service_instances/{instanceId}",1))
//                .andExpect(status().isOk())
//                .andDo(document("index", pathParameters( 
//            			parameterWithName("instanceId").description("The ID of the instance that will be created"))));
//    }
	
}
