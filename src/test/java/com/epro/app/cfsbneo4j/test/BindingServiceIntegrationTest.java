package com.epro.app.cfsbneo4j.test;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.junit.Assert;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.epro.app.cfsbneo4j.controller.ServiceBindingController;
import com.epro.app.cfsbneo4j.controller.ServiceInstanceController;
import com.epro.app.cfsbneo4j.model.ServiceInstanceRequest;

/**
 * @author Tobias, Yacine, Eric
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@ContextConfiguration(classes = IntegrationTestConfig.class)
@ComponentScan(basePackages = { "com.epro.app.cfsbneo4j"})
public class BindingServiceIntegrationTest {
	
	@Autowired
	private ServiceInstanceController sic;
	
	@Autowired
	private ServiceBindingController sbc;
	
	private String instanceId;
	
	private List<String> bindingIds;
	
	private String[] flavorIDs;
	
	private Random random;
	
	@Before
	public void createServer() {
		instanceId = "1234";
		bindingIds = new ArrayList<String>();
		
		for(int counter = 0; counter < 3; counter++) {
			bindingIds.add("" + counter);
		}
		
		random = new Random();
		flavorIDs = new String[] {"neo4j-s", "neo4j-m", "neo4j-xl", "neo4j-xxxl", "", null};
	}
	
	@Test
	public void createUser() {
		Assert.assertTrue(sic.createServiceInstance(instanceId, 
				new ServiceInstanceRequest(flavorIDs[random.nextInt(flavorIDs.length)])).getStatusCode().equals(HttpStatus.CREATED));
		
		for(String bindingId : bindingIds) {
			Assert.assertNotNull(sbc.update(instanceId, bindingId));
		}
	}
	
	@Test
	public void deleteUser() {
		for(String bindingId : bindingIds) {
			Assert.assertTrue(sbc.destroy(instanceId, bindingId).getStatusCode().equals(HttpStatus.OK));
		}
		Assert.assertTrue(sic.destroy(instanceId).getStatusCode().equals(HttpStatus.OK));
	}
}
