package com.epro.app.cfsbneo4j.test;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.junit.Assert;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.epro.app.cfsbneo4j.controller.ServiceInstanceController;
import com.epro.app.cfsbneo4j.model.ServiceInstance;
import com.epro.app.cfsbneo4j.model.ServiceInstanceRequest;

/**
 * @author Tobias, Yacine, Eric
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@ContextConfiguration(classes = IntegrationTestConfig.class)
@ComponentScan(basePackages = { "com.epro.app.cfsbneo4j"})
public class InstanceServiceIntegrationTest {
	
	@Autowired
	private ServiceInstanceController sic;
	
	private String[] flavorIDs;
	
	private List<String> instanceIds;
	
	private Random random;
	
	@Before
	public void initial() {
		instanceIds = new ArrayList<String>();
		
		for(int counter = 0; counter < 3; counter++) {
			instanceIds.add("" + counter);
		}
		
		random = new Random();
		flavorIDs = new String[] {"neo4j-s", "neo4j-m", "neo4j-xl", "neo4j-xxxl", "", null};
	}
	
	@Test
	public void createInstances() {
		for(String instanceId : instanceIds) {
			Assert.assertTrue(sic.createServiceInstance(instanceId, 
					new ServiceInstanceRequest(flavorIDs[random.nextInt(flavorIDs.length)])).getStatusCode().equals(HttpStatus.CREATED));
		}
		Assert.assertTrue(instanceIds.size() == ServiceInstance.serviceInstanceContainer.size());
	}
	
	@Test
	public void deleteInstances() {
		for(String instanceId : instanceIds) {
			Assert.assertTrue(sic.destroy(instanceId).getStatusCode().equals(HttpStatus.OK));
		}
		Assert.assertTrue(ServiceInstance.serviceInstanceContainer.size() == 0);
	}
}
