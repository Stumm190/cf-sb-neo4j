package com.epro.app.cfsbneo4j.test;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

import com.epro.app.cfsbneo4j.controller.ServiceBindingController;
import com.epro.app.cfsbneo4j.controller.ServiceInstanceController;
import com.epro.app.cfsbneo4j.deployment.OpenstackPlatformService;

/**
 * @author Tobias, Yacine, Eric
 *
 */
@ComponentScan(basePackages = { "com.epro.app.cfsbneo4j"})
public class IntegrationTestConfig {
	@Bean
	public OpenstackPlatformService openstackPlatformService(){
		return new OpenstackPlatformService();
	}
	
	@Bean
	public ServiceBindingController serviceBindingController() {
		return new ServiceBindingController();
	}
	
	@Bean
	public ServiceInstanceController serviceInstanceController() {
		return new ServiceInstanceController();
	}
}
